import { Avatar } from "@material-ui/core";
import { AccessTime, HelpOutline, Search } from "@material-ui/icons";
import { signOut } from "firebase/auth";
import React from "react";
import { useAuthState } from "react-firebase9-hooks/auth";
import styled from "styled-components";
import { auth } from "../firebase";

function Header() {
	const [user] = useAuthState(auth);

	return (
		<HeaderContainer>
			<HeaderLeft>
				<HeaderAvatar
					alt={user?.displayName}
					src={user?.photoURL}
					onClick={() => signOut(auth)}
				/>
				<AccessTime />
			</HeaderLeft>

			<HeaderSearch>
				<Search />
				<input type="text" placeholder="Search..." />
			</HeaderSearch>

			<HeaderRight>
				<HelpOutline />
			</HeaderRight>
		</HeaderContainer>
	);
}

export default Header;

const HeaderContainer = styled.div`
	display: flex;
	position: fixed;
	width: 100%;
	align-items: center;
	justify-content: space-between;
	padding: 10px 0;
	background-color: var(--slack-color);
	color: white;
`;

const HeaderLeft = styled.div`
	flex: 0.3;
	display: flex;
	align-items: center;
	margin-left: 20px;

	> .MuiSvgIcon-root {
		margin-left: auto;
		margin-right: 30px;
	}
`;

const HeaderAvatar = styled(Avatar)`
	cursor: pointer;
	:hover {
		opacity: 0.8;
	}
`;

const HeaderSearch = styled.div`
	flex: 0.4;
	opacity: 1;
	border-radius: 6px;
	background-color: #421f44;
	text-align: center;
	display: flex;
	padding-left: 5px;
	color: gray;
	border: 1px gray solid;

	> input {
		background-color: transparent;
		border: none;
		text-align: center;
		min-width: 30vw;
		width: 100%;
		outline: none;
		color: white;
	}
`;

const HeaderRight = styled.div`
	flex: 0.3;
	display: flex;
	align-items: flex-end;

	> .MuiSvgIcon-root {
		margin-left: auto;
		margin-right: 20px;
	}
`;
