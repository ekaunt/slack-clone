import { InfoOutlined, StarBorderOutlined } from "@material-ui/icons";
import { collection, doc, orderBy, query } from "firebase/firestore";
import React, { useEffect, useRef } from "react";
import { useCollection, useDocument } from "react-firebase9-hooks/firestore";
import { useSelector } from "react-redux";
import styled from "styled-components";
import { selectRoomId } from "../features/appSlice";
import { db } from "../firebase";
import ChatInput from "./ChatInput";
import Message from "./Message";

function Chat() {
	const chatRef = useRef(null);
	const roomId = useSelector(selectRoomId);
	const [roomDetails] = useDocument(roomId && doc(db, "rooms", roomId));
	const [roomMessages, loading] = useCollection(
		roomId &&
			query(
				collection(db, "rooms", roomId, "messages"),
				orderBy("timestamp", "asc")
			)
	);

	useEffect(() => {
		console.log(chatRef?.current);
		chatRef?.current?.scrollIntoView({ behavior: "smooth" });
	}, [roomId]);

	return (
		<ChatContainer>
			{roomDetails && roomMessages && (
				<>
					<Header>
						<HeaderLeft>
							<h4>
								<strong>#{roomDetails?.data().name}</strong>
							</h4>
							<StarBorderOutlined />
						</HeaderLeft>

						<HeaderRight>
							<p>
								<InfoOutlined />
								Details
							</p>
						</HeaderRight>
					</Header>

					<ChatMessages>
						{roomMessages?.docs.map((doc) => {
							const { message, timestamp, user, userImage } = doc.data();
							return (
								<Message
									key={doc.id}
									message={message}
									timestamp={timestamp}
									user={user}
									userImage={userImage}
								/>
							);
						})}
						<ChatBottom ref={chatRef} />
					</ChatMessages>

					<ChatInput
						chatRef={chatRef}
						channelName={roomDetails?.data().name}
						channelId={roomId}
					/>
				</>
			)}
		</ChatContainer>
	);
}

export default Chat;

const ChatContainer = styled.div`
	flex: 0.7;
	flex-grow: 1;
	overflow-y: scroll;
	margin-top: 60px;
`;

const Header = styled.div`
	display: flex;
	justify-content: space-between;
	padding: 20px;
	border-bottom: 1px solid lightgray;
`;

const HeaderLeft = styled.div`
	display: flex;
	align-items: center;

	> h4 {
		display: flex;
		text-transform: lowercase;
	}

	> .MuiSvgIcon-root {
		margin-left: 10px;
		font-size: 18px;
	}
`;
const HeaderRight = styled.div`
	> p {
		display: flex;
		align-items: center;
		font-size: 14px;
	}
	> p > .MuiSvgIcon-root {
		margin-right: 5px !important;
		font-size: 16px;
	}
`;

const ChatMessages = styled.div``;

const ChatBottom = styled.div`
	height: 200px;
`;
