import { Button } from "@material-ui/core";
import { addDoc, collection, serverTimestamp } from "firebase/firestore";
import React, { useRef } from "react";
import { useAuthState } from "react-firebase9-hooks/auth";
import styled from "styled-components";
import { auth, db } from "../firebase";

function ChatInput({ channelName, channelId, chatRef }) {
	const inputRef = useRef(null);
	const [user] = useAuthState(auth);

	const sendMessage = (e) => {
		e.preventDefault();

		chatRef.current.scrollIntoView({ behavior: "smooth" });

		if (!channelId || !inputRef.current.value) return false;

		addDoc(collection(db, "rooms", channelId, "messages"), {
			message: inputRef.current.value,
			timestamp: serverTimestamp(),
			user: user.displayName,
			userImage: user.photoURL,
		});

		inputRef.current.value = "";
	};

	return (
		<ChatInputContainer>
			<form>
				<input
					type="text"
					ref={inputRef}
					placeholder={`Message #${channelName}`}
				/>
				<Button hidden type="submit" onClick={sendMessage}>
					SEND
				</Button>
			</form>
		</ChatInputContainer>
	);
}

export default ChatInput;

const ChatInputContainer = styled.div`
	border-radius: 20px;

	> form {
		position: relative;
		display: flex;
		justify-content: center;
	}

	> form > input {
		position: fixed;
		bottom: 30px;
		width: 60%;
		border: 1px solid gray;
		border-radius: 3px;
		padding: 20px;
		outline: none;
	}

	> form > button {
		display: none !important;
	}
`;
