import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
import { getAuth, GoogleAuthProvider } from "firebase/auth";
// Your web app's Firebase configuration
const firebaseConfig = {
	apiKey: "AIzaSyArwKiXjoD7S-96JfZrnGi3ZeLPd8iO6to",
	authDomain: "slack-clone-70f3c.firebaseapp.com",
	projectId: "slack-clone-70f3c",
	storageBucket: "slack-clone-70f3c.appspot.com",
	messagingSenderId: "784633586303",
	appId: "1:784633586303:web:4b7dfa450e8302f9cc954a",
};

// Initialize Firebase
initializeApp(firebaseConfig);
const db = getFirestore();
const auth = getAuth();
const provider = new GoogleAuthProvider();

export { db, auth, provider };
